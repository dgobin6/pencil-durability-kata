# Pencil Kata Implementation

## Introduction

This project implements the Pencil Kata for Pillar Technology. It was written by Derek Gobin in C# on Visual Studio 2017 targeting .NET framework 4.6.1. There are two projects in this folder:

* pencilSimLib: This is the library that holds the Pencil class that performs the simple simulation of a pencil.

* PencilTest:  This is a set of tests written to drive development of the Pencil class. It uses Visual Studio's unit testing framework.  

## Quick Start

In case you don't have time to do a deep dive into all of the documentation, I put the instructions to build the solution and run the tests right at the beginning! In order to build this solution, you will need Visual Studio installed and the .NET framework.

### Command Line

Often the easiest way to do things is from the command line. 

First step is to make sure the new enviornment has all the needed dependencies. The only one used is the MS Test framework, which is a nuget package. To make this easy, we use the nuget.exe program included in the repository. 

Open up your administrator command prompt and type: 

`<pathToLocalRepository>\nuget.exe restore <pathToLocalRepository>\PencilDurabilityKata\PencilDurabilityKata.sln`

This command makes sure the test framework is ready to go on your system. 

Next we want to build our solution. To build a C# solution from the command line, we will use the MSBuild.exe tool. By default, it is installed with the .NET framework and located in the .NET folder. 

Go back to your command prompt and navigate to the MSBuild tool using the "cd" command. 

`cd C:\Windows\Microsoft.NET\Framework\v4.0.30319`

Once there, we can use our msbuild tool to build our solution file: 

`msbuild.exe <solution file> /p:Configuration=Release`

This will build both projects in our solution and create the respective .dll files in the projects' Release folders. 

![command prompt image](/DocumentationResources/buildCommandLine.PNG)

Next step is to run the tests. This requires a different tool: Microsoft's VSTest.Console.exe. This is typically located in the TestWindow folder of the Visual Studio directory. Again, use the cd command to switch to the appropriate directory. 

`cd "C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\Common7\IDE\CommonExtensions\Microsoft\TestWindow"`

To run the tests, we type:
 
`VSTest.Console.exe <path to PencilTest.dll>`

Once you hit enter, you should see all the test pass successfully. 

![command prompt test](/DocumentationResources/testCommandLine.PNG)

### Visual Studio

You can also build and run these tests in Visual Studio. First, open Visual Studio. Then in the top left corner, go to "File" and "Open". Select the PencilDurabilityKata solution file. 

Once the solution has loaded, go to the top menu bar again, this time selecting "Build" and then "Build Solution". Both projects will then be built.

Once completed, you can now select "Test" from the top menu bar, hover over the "Run" option, and select the "All Tests" option from the sub-menu. This will run all of the unit tests and open a Test Explorer column on the left-hand side of the solution window. The result of each test will be clearly displayed. 

## Documentation

Now a bit more of an in-depth look into the projects and their contents. 

### General Notes

* Throughout the code, there are comment sections marked with an "EC:". These stand for "evaluator comments" and are added to help explain a design decision or an assumption made during the project. They are separate from other comments that are more typical of what I would put in a production environment. I wanted them separate so that I could clearly state my choices to the audience without tainting too much the image I am creating of my typical code. 

* On Test Driven Development: This was my first time working in a TDD environment, and I am sure that shows in my code and commits. I tried my best to adhere to the design practice of building tests and then writing code for that test. A couple times, I was writing a method for the written test and realized my test was incorrect or inadequate and had to go back and change my test. If I were more disciplined in TDD, this probably would have occurred less. Any feedback on strengthening my code and TDD construction is appreciated. 

* On My Git Commits: I typically do not commit "broken" or otherwise non-working code. The TDD video indicated that commits should occur upon a test working, which I did. However, I also committed when a test was built and not working. This was because the Kata instructions indicated that I should commit frequently to show my thought process. I did my best to adhere to that, even if it comes off as a wonky production-level coding practice. 

### PencilTest

The PencilTest class was built to drive development of the Pencil class. This means that a test was built first to fail, and then the corresponding code in the Pencil class needed to make the test pass was written. As tests were completed, some refactoring took place to ensure the Pencil class was written cleanly and that tests were up to date as function structures changed to accommodate new features. 

The project specification and my code often refer to the "paper" that is being written on. These were Strings in my development code. 

The test class holds ten tests in all. Each is listed here with a short summary of what feature the test was meant to check for: 

* WriteToBlankSheetWithInfinitePencil(): This test used a pencil that did not degrade to write to a sheet of paper that started off blank. This was the most basic functionality as described by the "write" feature in the project specification. 

* WriteToNotBlankSheetWithInfinitePencil(): Our next test was to write to a sheet of paper that already had writing on it. This was to make sure we appended statements as directed. 

* WriteWithADegradingPencil(): The next feature we wanted to test for was that pencils could be specified with a durability that determined how much a pencil could write. This test was a straightforward example of this functionality. Please note that the method title refers to the pencil degrading with use, not that it is a pencil which insults its users. 

* WriteMoreComplicatedStuffWithADegradingPencil(): A second test was built to test some of the finer features outlined by the durability rules. These were mainly edge cases to ensure the functionality was fully implemented and that the pencil did not degrade on new line or other formatting characters.

* sharpenAPencilToKeepWriting(): Next was the sharpen feature. This test checks that the durability of a pencil can be refreshed, but only a limited number of times. 

* eraseSomeTextFromPaper(): This first test for the eraser feature makes sure we are erasing text from a paper correctly. 

* eraseWithDegradation(): The next eraser test checked for degradation of the eraser with use. It also made sure that we did not attempt to erase spaces or degrade when attempting to erase over a space. 

* editingSpaceLeftByEraser(): This test checks for the ability to overwrite white space made by the eraser through editing. This was only the basic check for editing e.g. words that fit cleanly into the space left behind.

* testCollisionEdits(): The next test method checks that collisions are handled correctly when we overwrite our editing space. It also makes sure we are handling degradation of graphite correctly while writing in the blanks. 

* testLongEdits(): This final test ensures that if we overwrite our eraser space at the current end of the paper, the new text is appended without issue. 

### Pencil Class

The pencilSimLib is a simple library containing only the class Pencil. This class contains methods to implement the features described in the Pencil Kata e.g. write, sharpen, erase, etc. 

The Pencil class consists of a constructor, four public methods, and one private method. Each is described briefly below. There are also 5 private properties associated with the pencil class. 

* Properties: The properties are durability (which dictates how much a pencil can write), originalDurability (which indicates how much durability a pencil starts with), pencilLength (sets the number of times a pencil can be renewed by sharpening), eraser durability (which determines how much we can erase with this pencil), and a queue of indexes called eraserSpaceIndexes. This final property keeps track of where an erased segment is located so that it can be written over during an edit call. 

* Pencil (constructor): Can take in a value to designate durability, length, and eraser durability. If a value is not designated, it is assumed to be infinite. Values that are less than -1 are assumed to designate a "broken" attribute which is equivalent to a value of 0. 

* Write: The write method takes in a reference to a string (here referred to as the paper) and text to write to the paper. The text is appended to the end of the paper. No value is returned, instead the passed in "paper" is altered directly by the write method. Writing a lowercase letter, number, or special character consumes 1 durability of the pencil. Capital letters consume 2 durability. Spaces/formatting characters consume no durability. 

*  Sharpen: The sharpen method takes in no inputs and returns no values. It sets the durability property back to the original durability value (stored in the originalDurability property) and decrements the length of the pencil. 

* Erase: The erase method takes in a reference to a string ("Paper") and text to erase. The method attempts to find the text on the passed in paper and replace it with blank spaces. Again, no value is returned, and the method instead directly updates the passed in string. If the text cannot be found on the paper, no action is taken. If the text exists in more than one place on the paper, only the last occurrence is erased. Characters are removed left-to-right and each character erased decrements the eraser durability by one. When durability hits zero, no more erasing can occur. Newline characters and spacing formatters (e.g. \t) are not erased and do not remove a durability when encountered. If an eraser runs out mid-operation, the text remains only partially removed. Once complete, the index of the last character to be erased is pushed to the eraserSpaceIndexes queue discussed above. 

* Edit: The edit process allows us to write to a blank space left by erasing with a new set of text. The method takes in a reference to a string and the new text to write. Editing only takes place with the first erased space being the first space overwritten, the index of which is pulled from our eraserSpaceIndexes queue. Using indexes to specify where to overwrite was found to be confusing notationally and offer little increased value. Values are written in directly over blank spaces; the text does not shift to accommodate the new text. When the input text is larger than the erased space, collisions can occur which are marked with "@" symbols. Collision characters and other written characters use pencil durability. A pencil with no durability cannot make edits. 

* writePaperDurabilityHandler: This private method takes in a character and returns a Boolean value. It was added during refactoring. The method checks the input character and the current durability. If the character cannot be written, a value of "false" is returned. If the character can be written, the durability is decremented appropriately and a value of "true" is returned. 
