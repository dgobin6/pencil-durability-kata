﻿/*
 * Class built test our Pencil class
 * C# generally requires a separate project for unit testing
 */

using System;
using pencilSimLib; 
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace PencilTest
{
    /*EC: I debated here a little bit between making a separate test class for each feature
     * but ultimately decided that would be a little verbose. Instead, I will have a test class
     * and a method for each feature I want to test. This is also more closely aligned with 
     * the TDD video example
     */
     
    [TestClass]
    public class pencilTestClass
    {
        //items used across tests
        Pencil infinitePencil;
                
        //initialize items used across tests
        [TestInitialize]
        public void TestInitialize()
        {
            infinitePencil = new Pencil(); 
        }

        //First test for our writing feature of our pencil.
        //For an "infinite" pencil e.g. one without durability
        [TestMethod]
        public void WriteToBlankSheetWithInfinitePencil()
        {
            //use our generic infinite pencil
            //initialize paper
            /*EC: Considered making paper a separate class in my library, but since it would only 
             * ever be just a container for a string, I decided it was overkill */
            String startingBlankPaper = ""; 
            //test writing function which writes to the "paper" with our pencil
            infinitePencil.Write(ref startingBlankPaper, "She sells");
            infinitePencil.Write(ref startingBlankPaper, " sea shells");
            //check result: Assert.AreEqual(expected, given); 
            Assert.AreEqual("She sells sea shells", startingBlankPaper); 

        }

        //now we want to test writing again with an infinite pencil
        //but this time we will test the ability to append e.g. writing to 
        //paper that already has stuff written to it
        [TestMethod]
        public void WriteToNotBlankSheetWithInfinitePencil()
        {
            //use our generic infinitePencil 
            //initialize paper
            String paperWithWriting = "The quick brown fox ";
            //now write to our paper
            infinitePencil.Write(ref paperWithWriting, "jumps over the lazy dog");
            //did we append? 
            Assert.AreEqual("The quick brown fox jumps over the lazy dog", paperWithWriting); 
        }

        //test method for testing degradation of graphite. 
        [TestMethod]
        public void WriteWithADegradingPencil()
        {
            //start with a pencil with a durability score
            Pencil lowDurabilityPencil = new Pencil(7, 1);
            String startingBlankPaper = "";

            //now we write to the paper, but we shouldn't get very far
            lowDurabilityPencil.Write(ref startingBlankPaper, "Hello, World!");

            //since durability should wear out, we will have some blanks
            Assert.AreEqual("Hello,       ", startingBlankPaper); 
        }

        //Now we have another method for testing degradation that goes 
        //into some weirder cases, such as durability going below infinity threshold
        //or experinecing new line characters
        [TestMethod]
        public void WriteMoreComplicatedStuffWithADegradingPencil()
        {
            //paper
            String startingBlankPaper = "";
            String anotherBlankSheet = "";
            String anotherAnotherSheet = ""; 

            //low durability pencil, high durability pencil, broken pencil
            Pencil lowDurabilityPencil = new Pencil(7, 1);
            Pencil highDurabilityPencil = new Pencil(200, 1);
            Pencil brokenPencil = new Pencil(-5, 1); 

            //write some weird stuff with low durability
            lowDurabilityPencil.Write(ref startingBlankPaper, "Hi\n\t\t    !#$");
            lowDurabilityPencil.Write(ref startingBlankPaper, "\nC"); //durability should now be at -1

            lowDurabilityPencil.Write(ref startingBlankPaper, "\ntesting");

            //high durability test
            highDurabilityPencil.Write(ref anotherBlankSheet, "~!@#$%^&*()>?_+=.,`");
            highDurabilityPencil.Write(ref anotherBlankSheet, " hello!");

            //broken test
            brokenPencil.Write(ref anotherAnotherSheet, "testing"); 

            /*EC: Note here that at durability 1, I cannot write C (which takes 
             * two durability as a capital letter) but I can write "t" from testing.
             * I assume this is the proper way to handle this sequence of inputs, although
             * it was not strictly said in the kata instructions (as opposed to the alternative
             * of stopping writting at the first character that cannot be written because we "wore out"
             * mid-character)
             */
             
            Assert.AreEqual("Hi\n\t\t    !#$\n \nt      ", startingBlankPaper);
            //high durability pencil should write everything out no issues. 
            Assert.AreEqual("~!@#$%^&*()>?_+=.,` hello!", anotherBlankSheet);
            //broken pencil should not write anything
            Assert.AreEqual("       ", anotherAnotherSheet); 
        }

        //Now to test the sharpen feature
        //Sharpen resets our durability, but only so many times due to length
        [TestMethod]
        public void sharpenAPencilToKeepWriting()
        {
            Pencil lowDurabilityPencil = new Pencil(7, 2);
            Pencil brokenPencil = new Pencil(500, -2); 
            

            String blankPaper = "";
            String nextSheet = "";

            lowDurabilityPencil.Write(ref blankPaper, "Hello,");
            lowDurabilityPencil.Sharpen();
            lowDurabilityPencil.Write(ref blankPaper, " World!");
            lowDurabilityPencil.Sharpen();
            lowDurabilityPencil.Write(ref blankPaper, "\nMORe");
            lowDurabilityPencil.Sharpen();
            lowDurabilityPencil.Write(ref blankPaper, " testing");

            brokenPencil.Write(ref nextSheet, "test more");

            Assert.AreEqual("Hello, World!\nMORe        ", blankPaper);
            //broken pencils can't write
            Assert.AreEqual("         ", nextSheet); 
        }

        //now to test the erase feature
        //A pencil should be able to erase text
        [TestMethod]
        public void eraseSomeTextFromPaper()
        {
            String sheetOfPaper = "This is used for testing the eraser";

            //use infinite pencil, since we are not testing durability here
            infinitePencil.Erase(ref sheetOfPaper, "eraser");

            //white space should be preserved after erasing it
            infinitePencil.Write(ref sheetOfPaper, " the test");

            //good so far? 
            Assert.AreEqual("This is used for testing the        the test", sheetOfPaper);

            //now erase test twice
            infinitePencil.Erase(ref sheetOfPaper, "test");
            infinitePencil.Erase(ref sheetOfPaper, "test");

            Assert.AreEqual("This is used for     ing the        the     ", sheetOfPaper); 

        }

        //now a second erase to test eraser degradation feature
        [TestMethod]
        public void eraseWithDegradation()
        {
            String sheetOfPaper = "A paper with writing on it";
            String nextSheet = "This contains\t\nNon-erasabe text"; 

            Pencil noEraserPencil = new Pencil(500, 500, -4);
            Pencil smallEraserPencil = new Pencil(500, 500, 3);

            noEraserPencil.Erase(ref sheetOfPaper, "it");

            //should do nothing
            Assert.AreEqual("A paper with writing on it", sheetOfPaper);

            //first it - note we can't actually erase the leading space
            //in other words, it shouldn't effect eraser durability
            smallEraserPencil.Erase(ref sheetOfPaper, " it");
            //second it
            smallEraserPencil.Erase(ref sheetOfPaper, "it");

            //attempt to erase something else
            smallEraserPencil.Erase(ref sheetOfPaper, "paper");

            //only the first it and the t of the second it shoud be erased
            Assert.AreEqual("A paper with wri ing on   ", sheetOfPaper);

            //just use infinte pencil, this makes sure we cannot erase spaces
            infinitePencil.Erase(ref nextSheet, "contains\t\n");
            Assert.AreEqual("This         \t\nNon-erasabe text", nextSheet); 


        }

        //Now we need to test the editing feature to fill in white space
        //left after using an eraser
        [TestMethod]
        public void editingSpaceLeftByEraser()
        {
            String paperSheetForEditing = "An apple a day keeps the doctor away";
            String paperSheetForPartialEdits = "We need another sheet"; 

            Pencil smallEraserPencil = new Pencil(500, 500, 7);
            Pencil bigEraserPencil = new Pencil(500, 500, 500);

            //first we will test that we can write to a specific part of the sheet
            bigEraserPencil.Erase(ref paperSheetForEditing, "apple");
            bigEraserPencil.Erase(ref paperSheetForEditing, "doctor");

            /*EC: I tried to build a system that took the index to write to, however
             * this proved confusing and complicated from a user perspective. Rather, 
             * I believe it is better to use FIFO structure and simply replace the 
             * oldest white space in the text. 
             */

            //Edit will replace the oldest whitespace in text
            bigEraserPencil.Edit(ref paperSheetForEditing, "onion");
            bigEraserPencil.Edit(ref paperSheetForEditing, "ogre");

            smallEraserPencil.Erase(ref paperSheetForPartialEdits, "need");
            smallEraserPencil.Erase(ref paperSheetForPartialEdits, "sheet");

            smallEraserPencil.Edit(ref paperSheetForPartialEdits, "want");
            smallEraserPencil.Edit(ref paperSheetForPartialEdits, "ip");

            //onion replaces apple, ogre replaces doctor
            Assert.AreEqual("An onion a day keeps the ogre   away", paperSheetForEditing);
            //want should replace need and ip should replace eet (which is as much of sheet 
            //as will be erased with our limited eraser)
            Assert.AreEqual("We want another ship ", paperSheetForPartialEdits); 
        }

        //now we have another edit test for the collision feature and check durability
        //while making edits
        [TestMethod]
        public void testCollisionEdits()
        {
            String paperForEdits = "Remember, Remember the fifth of Novemeber";

            Pencil smallPencil = new Pencil(10, 100, 100);

            smallPencil.Erase(ref paperForEdits, "fifth");
            smallPencil.Edit(ref paperForEdits, "fifteenth"); 
            smallPencil.Erase(ref paperForEdits, "Remember");
            smallPencil.Edit(ref paperForEdits, "Complete");

            Assert.AreEqual("Remember,  o       the fiftee@@hNovemeber", paperForEdits);
        }


        //a new test to check if edits don't go too far
        [TestMethod]
        public void testLongEdits()
        {
            String editPaper = "Long";

            infinitePencil.Erase(ref editPaper, "Long");
            infinitePencil.Edit(ref editPaper, "too long");

            Assert.AreEqual("too long", editPaper); 
        }
    }
}
