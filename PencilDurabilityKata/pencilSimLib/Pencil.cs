﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pencilSimLib
{
    public class Pencil
    {
        //durability property
        private int durability;
        private int originalDurability; 
        private int pencilLength;
        private int eraserDurability;
        private Queue<int> eraserSpaceIndexes; 

        /*EC: Just to point out an assumption I am making. The kata specifies that 
         * the pencil "can" take a durability value when being constructed. Not that 
         * it has to. This is why I have a separate constructor for durability vs no durability.
         * Durability also implies length. 
         * 
         * Similarly, a pencil "can" take eraser durability. Thus a third constructor is added. 
         * If eraser durability is not specified, it is assumed to have an infinite eraser.
         */ 

        //Constructor for a pencil
        //if a parameter is not input by a user, the default value of -1 is used
        //value of -1 is infinite
        public Pencil(int valueForDurability=-1, int valueForLength=-1, int valueforEraserDurability=-1)
        {
            if(valueForDurability < 0 && valueForDurability!=-1)
            {
                //cannot write
                valueForDurability = 0; 
            }

            if(valueForLength < 0 && valueForLength != -1)
            {
                //pencils with an invalid length are treated as broken, and thus cannot
                //write
                valueForLength = 0;
                valueForDurability = 0; 
            }
            if (valueforEraserDurability < 0 && valueforEraserDurability!=-1)
            {
                //can't erase
                valueforEraserDurability = 0; 
            }

            durability = valueForDurability;
            originalDurability = valueForDurability;
            pencilLength = valueForLength;
            eraserDurability = valueforEraserDurability;

            eraserSpaceIndexes = new Queue<int>();
        }

        //our method for using our pencil to write
        //Takes in a paper value and appends given text
        public void Write(ref String paper, String text)
        {

            if (durability == -1)
            {
                //this is an infinite pencil and we can just write the text
                paper += text;
            }else
            {
                //we need to take durability into account and the effect each character
                //has when writing
                //loop across each character so we can process durability for each
                foreach (char Character in text)
                {
                    bool toWriteOrNotToWrite = writePaperDurabilityHandler(Character); 
                    if(!toWriteOrNotToWrite)
                    {
                        //we did not have graphite to write a character
                        paper += ' '; 
                    }else
                    {
                        //we can write the character
                        paper += Character; 
                    }  
                    
                }
            }  
        }

        //the sharpen method simply resets durability to original durability
        //it can be called on infinite pencils, but it doesn't do anything
        public void Sharpen()
        {
            if(pencilLength >0)
            {
                //only can sharpen if we have pencil left
                pencilLength--;
                durability = originalDurability; 
            }
        }

        //this method erases the specified string from the paper sheet
        //in reverse order of when it appears. If a paper does not contain the text, 
        //no change is made
        public void Erase (ref String Paper, String textToErase)
        {
            //to erase text we get the index from paper of when the text last appeared
            int lastIndexOfTextToErase = Paper.LastIndexOf(textToErase); 

            if(lastIndexOfTextToErase != -1)
            {
                //at -1, the text does not exist on the paper
                //since it does exist, we need to remove it and replace it with blank spaces

                /* EC: Here I am using a method with "char array" rather than with stringbuilder.
                 * Some internet research indicates that char array is faster, even though you can
                 * get stringbuilder on one line. I think char array is easier to maintain as well
                 * since many languages have char arrays, but not everything has a stringbuilder
                 */

                Char[] PaperAsCharArray = Paper.ToCharArray();

                //start erasing at last index, and work backwards
                int startAtIndex = lastIndexOfTextToErase + textToErase.Length - 1;
                int stopAtIndex = lastIndexOfTextToErase;
                //an index to monitor our white space location for making edits
                int eraserIndexForEdits = startAtIndex; 
                for(int index= startAtIndex; index >= stopAtIndex; index--)
                {
                    if(PaperAsCharArray[index] > ' ' && PaperAsCharArray[index] <= '~')
                    {
                        //this is a writable character that we can erase 
                        if(eraserDurability == -1 || eraserDurability > 0)
                        {
                            PaperAsCharArray[index] = ' '; //replace with space
                            //update our eraser index with the latest index
                            eraserIndexForEdits = index; 
                            if(eraserDurability != -1)
                            {
                                eraserDurability--; //decrement eraser durability by one
                            }
                            
                        }
                    }
                    
                }
                eraserSpaceIndexes.Enqueue(eraserIndexForEdits); 
                Paper = new String(PaperAsCharArray); //convert back to string

            }

            
        }

        /*EC: I agonized a little bit over using a stringbuilder and the replace method
         * over the character array implemented below. Ultimately, I believe stringbuilder would be rendered
         * less optimal due to the need to account for and edit durability of a pencil 
         * (similar to why I didn't use it for erase and write) and because of collision
         * handling. I also looked for a way to use "write" method but found that the 
         * need to append vs write to a pre-existing index meant I would need to include two
         * functionalities in one method or use two methods for two funtionalities. I opted
         * for the latter. 
         */
         
        public void Edit(ref string Paper, String newTextToFill)
        {
            //we again will utilize the string as a char array for fast edits made
            //to the paper
            Char[] newTextToFillAsCharArray = newTextToFill.ToCharArray();

            //get our start and stopping points for the editing
            int startIndex = eraserSpaceIndexes.Dequeue();
            int stopIndex = startIndex + newTextToFill.Length;

            //check if we exceed the length of our allocated space
            while(stopIndex > Paper.Length)
            {
                //Add white spaces that will be replaced by our for loop with the 
                //new text
                Paper += " "; 
            }

            Char[] PaperAsCharArray = Paper.ToCharArray();

            for (int paperIndex = startIndex; paperIndex < stopIndex; paperIndex++)
            {
                char Character = newTextToFillAsCharArray[paperIndex - startIndex];

                //find out if we have the durability to write and update durability
                //note that I still take durability for the collision character
                //I assume it is still written by my "pencil" 
                bool canWeWrite = writePaperDurabilityHandler(Character); 

                //if we can write, we attempt to overwrite the current character, either with
                //the character we want to write or the collision character. If we have no graphite
                //left, nothing changes. Blank stays blank and characters we would overwrite, we don't
                if(canWeWrite)
                {
                    if (PaperAsCharArray[paperIndex] != ' ')
                    {
                        //we should only write over white spaces created by erasing
                        //everything else is a collision (including overwriting tabs
                        //and new lines, since that alters format)
                        PaperAsCharArray[paperIndex] = '@'; 
                    }else
                    {
                        PaperAsCharArray[paperIndex] = Character; 
                    }
                }

            }


            Paper = new String(PaperAsCharArray); //convert back to string

        }

        //a private method to handle durability deduction and checking when writing
        //with our pencil
        private bool writePaperDurabilityHandler(char characterToWrite)
        {
            if (durability == 0 || (characterToWrite >= 'A' && characterToWrite < 'Z' && durability < 2))
            {
                //we cannot write this character
                return false; 
            }else
            {
                //we can write the character and should update durability
                if (characterToWrite >= 'A' && characterToWrite <= 'Z')
                {
                    //capital letter, reduce by two
                    durability -= 2;
                }
                else if ((characterToWrite > ' ' && characterToWrite < 'A') || (characterToWrite > 'Z' && characterToWrite <= '~'))
                {
                    //character is still written, should impact durability by 1
                    durability--;
                }

                //remaining characters do not impact durability (e.g. space)
                return true; 
            }
        }
    }
}
